import { Params } from './../../api-client';
import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { ApiClient } from "../../api-client";
import { ApiService } from '../../api.service';
import { Todo } from '../../models/todo';

import Strapi from 'strapi-sdk-javascript';
import { AxiosRequestConfig } from 'axios';

import * as Minio from 'minio';
import { Service } from '../../models/service';

//var Minio = require('minio');

//const strapi = new Strapi('http://divstuff.com:1337');
const strapi = new Strapi('http://localhost:1337');


interface Friend {
	id: number;
	name: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  	public friends: Friend[];
	todos: Todo[];
	departmentServices: Service[];
	departmentDetail = {};
	personnel = [];
	socialMedia;

	constructor( private apiClient: ApiClient,
		private todoDataService: ApiService ) {

		this.friends = [];
		
		this.departmentServices = [];

		document.cookie = "XSRF-TOKEN=server-generated-token";
	}

  async ngOnInit() {
	
		await strapi.login('admin01', 'Password1')
					.then(result => {
						console.log(result);
					});
			
		let params = JSON.stringify({
			params: {
				limit: 1
			}
		});

		// const userx = await strapi.getEntries('article', params);
		// const user = await strapi.getEntry('article', '5b0b9277e6904a630765587c');
		const services = await strapi.getEntries('departmentservices', params);
		services.forEach(service => this.departmentServices.push(new Service(service)));
		
		this.departmentDetail = await strapi.getEntries('departmentdetail');
		this.personnel = await strapi.getEntries('personnel');
		this.socialMedia = await strapi.getEntries('socialmedia');
		
		// let link = this.socialMedia.forEach(x => x.id === 1);
		console.log(this.departmentServices)

		// this.loadFriends();
		// this.todoDataService.getAllTodos().subscribe(todos => {
		// 	console.log(todos);
		// 	this.todos = todos;
		// });
  }

	
  	public async loadFriends() : Promise<void> {

	// 	try {
	// 		this.friends = await this.apiClient.get<Friend[]>({
	// 			url: "http://localhost:3000/friends",
	// 			params: {
	// 				limit: 10
	// 			}
	// 		});

    //   console.log(this.friends);
	// 	} catch ( error ) {
	// 		console.error( error );
	// 	}
	}

	minioTest() {
		// var minioClient = new Minio.Client({
    //   endPoint: 'divstuff.com',
    //   port: 9000,
    //   secure: true,
    //   accessKey: 'admin',
    //   secretKey: 'Password1'
		// });

    // var minioClient = new Minio.Client({
    //   endPoint: 'localhost',
    //   port: 9000,
    //   secure: true,
    //   accessKey: 'admin',
    //   secretKey: 'Password1'
    // });

    // var file = '/assets/img/logo2x.png'

    // minioClient.makeBucket('test', 'us-east-1', function (err) {
    //   if (err) return console.log(err)

    //   console.log('Bucket created successfully in "us-east-1".')

    //   minioClient.fPutObject('test', 'logo2x.png', file, 'application/octet-stream', function (err, etag) {
    //     if (err) return console.log(err)
    //     console.log('File uploaded successfully.')
    //   });
    // });
  }
}
