import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  @Input() personnel;
  @Input() socialMedia;
  personnelDetail = {};
  executives = [];
  councilors = []
  constructor() {
    
  }

  ngOnInit() {
    this.sortPersonnel()
  }

  sortPersonnel(){
    this.councilors = this.personnel.filter(e => e.position === 'Councilor');
    this.executives = this.personnel.filter(e => e.position !== 'Councilor');
  }

  getPersonnelData(detail){
    this.personnelDetail = detail;
  }

  getSocialMedia(id){
    let i = this.socialMedia.find(x => x.id == id);
    return 'fa fa-' + i.type.toLowerCase();
  }


}
