import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';


import { HeaderComponent } from './header/header.component';
import { BannerComponent } from './banner/banner.component';
import { SiteMenuComponent } from './site-menu/site-menu.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderBlogComponent } from './header-blog/header-blog.component';
import { SiteMenuBlogComponent } from './site-menu-blog/site-menu-blog.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const SHARED_COMPONENTS = [
  HeaderComponent,
  BannerComponent,
  SiteMenuComponent,
  FooterComponent,
  HeaderBlogComponent,
  SiteMenuBlogComponent,
  PageNotFoundComponent,

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    ...SHARED_COMPONENTS,
  ],
  exports: [
    SHARED_COMPONENTS
  ]
})
export class SharedModule { }
