import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule }    from '@angular/common/http';

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './/app-routing.module';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { ServicesComponent } from './components/sections/services/services.component';
import { PortfolioComponent } from './components/sections/portfolio/portfolio.component';
import { AboutComponent } from './components/sections/about/about.component';
import { GalleryComponent } from './components/sections/gallery/gallery.component';
import { ClientsComponent } from './components/sections/clients/clients.component';
import { StoriesComponent } from './components/sections/stories/stories.component';
import { ContactComponent } from './components/sections/contact/contact.component';
import { HomeComponent } from './components/home/home.component';
import { BlogComponent } from './components/blog/blog.component';
import { AdminModule } from './components/admin/admin.module';

import * as axios from 'axios'
import { Http } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    ServicesComponent,
    PortfolioComponent,
    AboutComponent,
    GalleryComponent,
    ClientsComponent,
    StoriesComponent,
    ContactComponent,
    HomeComponent,
    BlogComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    AdminModule,
    SharedModule
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: Http, useValue: axios}
  ]
})
export class AppModule { }
