import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, Subject, ReplaySubject, from, of, range } from 'rxjs';
import { catchError, map, tap, filter, switchMap } from 'rxjs/operators';

import { Todo } from './models/todo';
import { MessageService } from './message.service';

const API_URL = environment.apiUrl + '/todos';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class ApiService {

    constructor(
        private http: HttpClient,
        private messageService: MessageService) {
    }

    /** GET todos from the server */
    getAllTodos(): Observable<Todo[]> {
        return this.http.get<Todo[]>(API_URL)
            .pipe(
                tap(todos => this.log(`fetched todos`)),
                catchError(this.handleError('getTodos', []))
            );
    }

    /** GET todo by id. Return `undefined` when id not found */
    getTodoNo404<Data>(id: number): Observable<Todo> {
        const url = `${API_URL}/?id=${id}`;
        return this.http.get<Todo[]>(url)
            .pipe(
                map(todos => todos[0]), // returns a {0|1} element array
                tap(h => {
                    const outcome = h ? `fetched` : `did not find`;
                    this.log(`${outcome} todo id=${id}`);
                }),
                catchError(this.handleError<Todo>(`getTodo id=${id}`))
            );
    }

    /** GET todo by id. Will 404 if id not found */
    getTodo(id: number): Observable<Todo> {
        const url = `${API_URL}/${id}`;
        return this.http.get<Todo>(url).pipe(
            tap(_ => this.log(`fetched todo id=${id}`)),
            catchError(this.handleError<Todo>(`getTodo id=${id}`))
        );
    }

    /* GET todos whose name contains search term */
    searchTodos(term: string): Observable<Todo[]> {
        if (!term.trim()) {
            // if not search term, return empty todo array.
            return of([]);
        }
        return this.http.get<Todo[]>(`${API_URL}/?name=${term}`).pipe(
            tap(_ => this.log(`found todos matching "${term}"`)),
            catchError(this.handleError<Todo[]>('searchTodos', []))
        );
    }



    //////// Save methods //////////

    /** POST: add a new todo to the server */
    add(todo: Todo): Observable<Todo> {
        return this.http.post<Todo>(API_URL, todo, httpOptions).pipe(
            tap((todo: Todo) => this.log(`added todo w/ id=${todo.id}`)),
            catchError(this.handleError<Todo>('add todo'))
        );
    }

    /** DELETE: delete the todo from the server */
    delete(todo: Todo | number): Observable<Todo> {
        const id = typeof todo === 'number' ? todo : todo.id;
        const url = `${API_URL}/${id}`;

        return this.http.delete<Todo>(url, httpOptions).pipe(
            tap(_ => this.log(`deleted todo id=${id}`)),
            catchError(this.handleError<Todo>('delete todo'))
        );
    }

    /** PUT: update the todo on the server */
    update(todo: Todo): Observable<any> {
        return this.http.put(API_URL, todo, httpOptions).pipe(
            tap(_ => this.log(`updated todo id=${todo.id}`)),
            catchError(this.handleError<any>('update todo'))
        );
    }

    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a HeroService message with the MessageService */
    private log(message: string) {
        this.messageService.add('HeroService: ' + message);
    }
}