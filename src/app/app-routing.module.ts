import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, ExtraOptions } from '@angular/router';
import { SharedModule } from './shared/shared.module';

import { HomeComponent } from './components/home/home.component';
import { BlogComponent } from './components/blog/blog.component';
import { ServicesComponent } from './components/sections/services/services.component';
import { AdminComponent } from './components/admin/admin.component';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'admin', 
    children: [
      {path: '', component: AdminComponent, pathMatch: 'full'},
      {path: 'blog', component: BlogComponent},
      {path: '**', redirectTo: '' }
    ]
  },
  { path: '**', component: PageNotFoundComponent }
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forRoot(routes, config),
  ],
  declarations: []
})
export class AppRoutingModule { }
