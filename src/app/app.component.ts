import { Component } from '@angular/core';

interface Friend {
	id: number;
	name: string;
}

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {
  title = 'app';
}
