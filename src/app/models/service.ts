export class Service {
    id: number = 1;
    title: string = 'string 1';
    description: string = 'string 2';
    cover: string = 'string 3';

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}