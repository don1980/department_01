# README #

Angular 6 using Analogue themes

yarn add global @angular/cli

# add this to use external javascript libraries 
-> https://www.techiediaries.com/use-external-javascript-libraries-typescript-projects/
npm install @types/node --save


## CMS

# https://github.com/amelki/cms-json
# npm install -g cms-json

cms-json -s cms/default/schema.json -d cms/data.json -p 3100

json-server --watch api/db.json


# use external libraries
# https://blog.strapi.io/release-v3-alpha-11-file-upload/
# https://github.com/strapi/strapi-sdk-javascript
# npm install strapi-sdk-javascript --save


import Strapi from 'strapi-sdk-javascript';

const strapi = new Strapi('http://divstuff.com:1337');


const form = new FormData();
form.append('files', fileInputElement.files[0], 'file-name.ext');
form.append('files', fileInputElement.files[1], 'file-2-name.ext');
const files = await strapi.upload(form);



# minio JS SDK
npm install --save-dev @types/minio

git fetch && git checkout master